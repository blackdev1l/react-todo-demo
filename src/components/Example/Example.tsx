import React from 'react';

interface ExampleProps {
    example?: string;
}

interface ExampleState {
    textExample: string;
}

export class Example extends React.Component<ExampleProps, ExampleState> {
    constructor(props: ExampleProps) {
        super(props);
        this.state = {
            textExample: '',
        };
    }

    handleChange(event: React.ChangeEvent<HTMLTextAreaElement>): void {
        this.setState({ textExample: event.target.value });
    }

    render(): JSX.Element {
        const { textExample } = this.state;
        return (
            <>
                <textarea
                    name="exampleArea"
                    id="areEx"
                    cols={30}
                    rows={2}
                    onChange={(event): void => {
                        this.handleChange(event);
                    }}
                />
                <h1>{textExample}</h1>
            </>
        );
    }
}
