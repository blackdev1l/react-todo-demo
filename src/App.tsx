import React from 'react';
import './App.scss';

const App: React.FC = () => {
    return (
        <div className="app">
            <header>
                <img src="https://www.iungo.com/wp-content/uploads/2019/10/logo-iungo.png" alt="" />
                <h2 className="title">Todo app</h2>
            </header>

            <div className="todo" />
        </div>
    );
};

export default App;
